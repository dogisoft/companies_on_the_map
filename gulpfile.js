var elixir = require('laravel-elixir');
var uglify = require('gulp-uglify');

require('laravel-elixir-vue-2');

elixir(function(mix) {
    mix.styles([    
        '/resources/assets/css/dev.css'
    ], 'public/css/app.css');
});

var concat = require('gulp-concat');
 
gulp.task('front-js', function() {
  return gulp.src([
      'resources/assets/js/bootstrap.min.js',       
      'resources/assets/js/jquery.magnific-popup.min.js', 
      'resources/assets/js/scrollreveal.min.js', 
      'resources/assets/js/creative.js', 
      'resources/assets/js/app.js' 
        ])
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js/'));
});


gulp.task('jquery-js', function() {
  return gulp.src([
      'resources/assets/js/jquery.min.js',       
      'resources/assets/js/jquery.ui.min.js'
        ])
    .pipe(concat('jquery.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js/'));
});




