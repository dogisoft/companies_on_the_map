<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/category/{letter}', 'HomeController@category');
Route::get('/companies/{category}', 'HomeController@companies');
Route::get('/company/{id}/{category}', 'HomeController@companies');
Route::get('/add-location/{id}', 'HomeController@addLocation');
Route::get('/ceg/{id}/{slug}', 'HomeController@ceg');

Route::get('/esemenyek', 'HomeController@blank');
Route::get('/kepek', 'HomeController@blank');
Route::get('/heti-menu', 'HomeController@blank');
Route::get('/esemenyek', 'HomeController@blank');
Route::get('/kereses', 'HomeController@blank');
Route::post('/kereses', 'HomeController@blank');

