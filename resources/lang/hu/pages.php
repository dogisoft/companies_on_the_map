<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Authentication Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used during authentication for various
      | messages that we need to display to the user. You are free to modify
      | these language lines according to your application's requirements.
      |
     */

    'email' => 'E-mail cím',
    'password' => 'Jelszó',
    'remember' => 'Jelszó megjegyzése',
    'login_btn_text' => 'Belépés',
    'forgott_pw' => 'Elfelejtette jelszavát?',
    'login' => 'Login',
    'register' => 'Regisztráció',
    'name' => 'Név',
    'password' => 'Jelszó',
    'pass_conf' => 'Jelszó megerősítése',
    'register_btn_text' => 'Regisztrálok',
    'home_title' => 'a legfrissebb IT pozíciók',
    'home_subtitle' => 'Nehéz feladat a megfelelő IT-s szakember kiválasztása. MI, ebben nyújtunk segítséget neked! Próbáld ki 30 napig ingyen!',
    'home_subtitle_btn' => 'Tovább >>',
    'home_big_orange_title' => 'A szakember már csak egy karnyújtásnyira',
    'home_big_orange_text' => 'Helyezd el hirdetésed az oldalunkon és dőlj hátra. ',
    'home_big_orange_btn' => 'Részletek',
    'home_services_title' => 'Egyszerű és átlátható',
    'home_services_h3_1' => 'Regisztrálj az oldalunkon',
    'home_services_h3_2' => 'Add fel a hirdetésed',
    'home_services_h3_3' => 'Várj a jelentkezőkre',
    'home_services_h3_4' => 'Válaszd ki a legjobbat és dönts okosan',
    'home_services_p_1' => 'Mindössze 2 percig tart és máris közzénk tartozol  ',
    'home_services_p_2' => 'A hirdetés feladása egyszerű és érthető bármely korosztály számára',
    'home_services_p_3' => 'Figyeld a leveleidet, mert rövidesen érkeznek a jelentkezések',
    'home_services_p_4' => 'Válaszd ki a legjobbat és valósítsd meg álmaidat',
];
