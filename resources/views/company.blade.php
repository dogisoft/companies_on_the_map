<?php

use App\Http\Controllers\HelperController;

$gps = explode(',', HelperController::kapcsolat($company_results->cid)->gps);
?>
<html>
    <head>
        <title>Cégkereső</title>
        <link href="/css/app.css" rel="stylesheet">     
        <script src="/js/jquery.js"></script>          
    </head>
    <body>

        @if($gps[0])      
        <script>
            var markers = [
            [{{$gps[1].','.$gps[0]}}]
            ];
        </script>
        @endif      
          
        
        <div class="top-menu">
            <div class="logo">
                <img src="/img/logo.png" />
            </div>
            
            <div class="mobile-menu menu">
                <a class="helyek" href="/">helyek</a>
                <a class="esemenyek" href="/esemenyek">események</a>
                <a class="kepek" href="/kepek">képek</a>
                <a class="hetimenu" href="/heti-menu">heti menü</a>
            </div> 
            
            <div class="menu">
                <a class="helyek" href="/">helyek</a>
                <a class="esemenyek" href="/esemenyek">események</a>
                <a class="kepek" href="/kepek">képek</a>
                <a class="hetimenu" href="/heti-menu">heti menü</a>
            </div>

            <div class="search">
                <span class="addNewPlace">Hely hozzáadása</span>
                <span><input id="keresesField" type="text" name="kereses" /> <a href="/kereses"><img id="searchButton" src="/img/search.png" /></a></span>
            </div>			
        </div>         

                <div class="left-menu">
                    @foreach($letters as $item)
                    <a class="menu-letters" href="/category/{{$item}}"><span>{{$item}}</span></a>
                    @endforeach                    
                </div>                 

                <div class="left-side">
                    <div>
                        <h1>{{$company_results->nev}}</h1>
                        <h4>{{$company_results->tevekenyseg}}</h4>
                        <p>{{$company_results->nev}}</p>

                        <p> {{HelperController::kapcsolat($company_results->cid)->ertek}}</p>
                        <p>{!!HelperController::nyitvatartas($company_results->cid)!!}</p>
                        <p>{!!HelperController::elerhetosegek($company_results->cid)!!}</p>
                    </div>

                </div>                                                                   


                <div class="right-side">
                    <div id="mapdiv"></div>
                    <button id="zoom-out">Zoom out</button>
                    <button id="zoom-in">Zoom in</button>                  
                </div>              
            
 

        <script src="http://www.openlayers.org/api/OpenLayers.js"></script> 
        <script src="/js/app.js"></script> 

    </body>
</html>
