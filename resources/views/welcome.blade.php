@include('head')       

        <div class="left-menu">
            @foreach($letters as $item)
            <a class="menu-letters" href="/category/{{$item}}"><span>{{$item}}</span></a>
            @endforeach                    
        </div>                

        <div class="left-side">                

            <div class="results">
                @if(isset($results))
                <h3>Találatok listája</h3>
                @foreach($results as $key => $item)
                
                @if($key==0)
                
                @else  | @endif
                
                <a href="/companies/{{$item->kid}}"><span>{{$item->kategorianeve}}</span></a> 
                @endforeach                                          
                @endif
                
            </div>

            <div class="company_results">
                @if(isset($company_results))
                @foreach($company_results as $item)
                <a href="/ceg/{{$item->cid}}/{{$item->slug}}">
                    <div class="companyitem">
                        <div>{{$item->nev}}</div>                                    
                        <p>{{str_limit($item->tevekenyseg, 70)}}</p>
                    </div>
                </a>
                @endforeach                          
                @endif
            </div>                      



        </div>

        <div class="right-side">
            <div id="mapdiv"></div>
            <button id="zoom-out">Zoom out</button>
            <button id="zoom-in">Zoom in</button>                  
        </div>              


        <script src="http://www.openlayers.org/api/OpenLayers.js"></script> 
        <script src="/js/app.js"></script> 

    </body></html>
