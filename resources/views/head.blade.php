<html>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <head>
        <title>Cégkereső</title>
        <link href="/css/app.css" rel="stylesheet">       
        <script src="/js/jquery.js"></script>          
    </head>
    <body>

        <div class="top-menu">
            <div class="logo">
                <img src="/img/logo.png" />
            </div>

            <div class="mobile-menu menu">
                <a class="helyek" href="/">&nbsp;</a>
                <a class="esemenyek" href="/esemenyek">&nbsp;</a>
                <a class="kepek" href="/kepek">&nbsp;</a>
                <a class="hetimenu" href="/heti-menu">&nbsp;</a>
            </div>            
            
            <div class="menu">
                <a class="helyek" href="/">helyek</a>
                <a class="esemenyek" href="/esemenyek">események</a>
                <a class="kepek" href="/kepek">képek</a>
                <a class="hetimenu" href="/heti-menu">heti menü</a>
            </div>

            <div class="search">
                <span class="addNewPlace">Hely hozzáadása</span>
                <span><input id="keresesField" type="text" name="kereses" /> <a href="/kereses"><img id="searchButton" src="/img/search.png" /></a></span>
            </div>			
        </div>  