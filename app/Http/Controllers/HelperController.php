<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;


class HelperController extends Controller
{

    public static function kapcsolat($id)
    {         
        $company = DB::table('kapcsolat')
                ->leftJoin('cegek_kapcsolat', 'cegek_kapcsolat.xid', '=', 'kapcsolat.xid')
                ->where('cegek_kapcsolat.cid', $id)
                ->first(); 
        
        return $company;
    }
    
    public static function nyitvatartas($id)
    {         
        $company = DB::table('nyitvatartas')
                ->leftJoin('cegek_nyitvatartas', 'cegek_nyitvatartas.nyid', '=', 'nyitvatartas.nyid')
                ->where('cegek_nyitvatartas.cid', $id)
                ->get(); 
        
        $com = "<h3>Nyitvatartás: </h3>";
        
        foreach($company as $kulcs => $ertek){
            $com .= "<p>".$ertek->name.": ";
            $com .= "".$ertek->ertek."</p>";
        }
        
        if(isset($kulcs)){
            return $com;
        }
        
    }  
    
    public static function elerhetosegek($id)
    {         
        $company = DB::table('elerhetosegek')
                ->leftJoin('cegek_elerhetosegek', 'cegek_elerhetosegek.eid', '=', 'elerhetosegek.eid')
                ->where('cegek_elerhetosegek.cid', $id)
                ->get(); 
        
        $com = "<h3>Elérhetőségek: </h3>";
        
        foreach($company as $kulcs => $ertek){
            $com .= "<p>".$ertek->name.": ";
            $com .= "".$ertek->ertek."</p>";
        }
        
        if(isset($kulcs)){
            return $com;
        }        
  
    }      
	
}
