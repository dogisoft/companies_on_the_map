<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
 
    
    public function index()
    {         
        $letters = array();
        
        foreach (range('A', 'Z') as $char) {
            array_push($letters, $char);
        }
        
        $category = DB::table('kategoriak')->where('kategorianeve', 'LIKE', 'A%')->get();
        
        return view('welcome', ['results' => $category, 'letters' => $letters]);
    }
    
    public function category($letter)
    {
        $letters = array();
        
        foreach (range('A', 'Z') as $char) {
            array_push($letters, $char);
        }
        
        $category = DB::table('kategoriak')->where('kategorianeve', 'LIKE', $letter.'%')->get();
        
        return view('welcome', ['results' => $category, 'letters' => $letters]);
    }

    public function companies($category)
    {
        $letters = array();
        
        foreach (range('A', 'Z') as $char) {
            array_push($letters, $char);
        }
        
        $company_results = DB::table('cegek_kategoriak')
                ->leftJoin('cegek', 'cegek_kategoriak.cid', '=', 'cegek.cid')
                ->where('kid', $category)
                ->get();       
                
        return view('welcome', ['company_results' => $company_results, 'letters' => $letters]);
    }  
    
    public function ceg($id, $slug)
    {   
        $letters = array();
        
        foreach (range('A', 'Z') as $char) {
            array_push($letters, $char);
        }
        
        $company_results = DB::table('cegek')
                ->where('cid', $id)
                ->first();            
             
        return view('company', ['letters' => $letters, 'company_results' => $company_results]);
    }      
    
    public function addLocation($id)
    {        
       $kapcsolatok = DB::table('kapcsolat')->where('xid', $id)->get();
       //s$kapcsolatok = DB::table('kapcsolat')->where('gps', '')->get();
        
        foreach($kapcsolatok as $kulcs => $ertek){
            
            $coor = HomeController::getCoordinates($ertek->ertek);
            if($coor==0){
                
            }
            else{
                DB::table('kapcsolat')->where('xid', $ertek->xid)->update(['gps' => $coor]);
            }
                     
        }
    }      
    
    
    public function getCoordinates($address){
        
        $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern

        $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";

        $response = file_get_contents($url);

        $json = json_decode($response,TRUE); //generate array object from the response from the web              
               
        if($json['status']=="ZERO_RESULTS"){
            return  0;
        }
        else{
            return ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);
        }
    }	
	
    
    public function blank()
    {         
        $letters = array();
        
        foreach (range('A', 'Z') as $char) {
            array_push($letters, $char);
        }       

        
        return view('blank', ['letters' => $letters]);
    }    
    
}
