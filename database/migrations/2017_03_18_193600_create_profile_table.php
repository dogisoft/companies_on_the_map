<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id'); 
			$table->string('company', 255);
			$table->string('contact_person', 255);
			$table->text('about');	
			$table->string('website', 100);
			$table->string('email', 100);
			$table->string('phone', 55);
			$table->integer('status'); 			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
