<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id'); 
			$table->integer('category_id'); 
			$table->string('title', 255);
			$table->text('description');
			$table->string('salary', 55);
			$table->string('job_url', 255);
			$table->string('contact_name', 100);
			$table->string('contact_email', 100);
			$table->string('contact_phone', 35);
			$table->date('valid_until');			
			$table->integer('status'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job');
    }
}
